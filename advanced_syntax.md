
# Advanced Python Syntax

## Decorators
A decorator is a function that can be `applied` to a class or function.  It takes that object as an argument and returns a modified version of that object.

Examples include dataclasses, retry, lru_cache, and classmethod.

```python
def this_is_a_decorator(func):

    def modified_func():
        printf("Func called!")
        func()

    return modified_func

@this_is_a_decorator
def a_function():
    pass

a_function() # prints "Func called!"
```

```python

@dataclass
class test_class
    name: str
    age: int

# Passes the original test_class class to the dataclass decorator function, replacing test_class with a modified version.
```

## Generators
A generator is a function that is used to iterate over its results. 
It maintains state between iterations and uses the 'yield' keyword 
to pause execution and return a value.

Iteration is complete when the `StopIteration` exception is raised. 
This usually means there are no more objects to loop over.

Example:
```python
"""
Let's replicate python's built-in 'range' generator function.
range(n) yields numbers from 0 to n-1, one at a time.
"""
list(range(10)) # [0,1,2,3,4,5,6,7,8,9]

def range(n: int) -> int:
    i = 0
    while i < n:
        yield i
        i += 1
    raise StopIteration # Signals that iteration is complete!
```

Generators can be `iterated` over like lists or other collections!

```python
for n in range(10):
    do_something(n)
```

## Comprehensions
You can quickly construct lists, sets, and other collections using `comprehensions`.

```python
squared_nums = [n * n for n in range(10)]
# [1,4,9,16,...,81]

multiples = {i * j for i in range(10) for j in range(10)}
# A set of all unique multiples of two numbers between 0 and 9 inclusively

numbers_and_strings = {num: str(num) for num in range(10)}
# A key-value dictionary of all numbers 0-9 paired with their string represnetations
```

## f-strings
Use `f-strings` to easily format string literals using any variable.

Older methods use the modulo operator (%):

```python
string1 = "quartz"
string2 = "judge"
print("Sphinx of black %s, %s my vow", (string1, string2))
# Sphinx of black quartz, judge my vow
```
... or the `format()` string method:

```python
print("Sphinx of black {}, {} my vow".format(string1, string2))
```

f-strings make this much simpler to write and read:

```python
print(f"Sphinx of black {string1}, {string2} my vow")
```

## Context Managers
Use context managers and the "with" keyword to easily handle cleanup tasks like closing files.

```python
with open('file_path.txt', 'r') as input_file:
    do_something()
# File is automatically closed after leaving this scope!
```

## Lambdas
Lambdas are short, anonymous functions that can be easily passed as function arguments.

```
# These two are *identical*

func1 = lambda x: x * 2

def func2(x):
    return x * 2
```

## 'Magic' Methods
You can use "magic methods" or "dunder methods" to re-define the behavior of your classes.
This includes things like overriding operators, handling boolean or string conversion, or even allowing your custom class to be indexed like a list or dictionary.

```python
# Some common dunder-methods

class my_example_class():

    # Called when using the + operator on this class
    def __add__(self, other):
        return self.number + other.number

    # Called when evaluating an object of this class as a boolean value
    def __bool__(self):
        return True

    # Called when comparing this object for equality
    def __eq__(self, other):
        return False
```

## JSON
It is simple to convert python data structures to and from JSON (and other serialized formats) for interacting with REST APIs or flat files.
As you can see from this example, the presentation is nearly identical:

```python
import json
user_data = {
    "username": "someone",
    "password": "password123",
    "login_count": 3,
    "roles": [
        "developer",
        "admin"
    ]
}
print(json.dumps(user_data, indent=4))
```

Output:
```json
{
    "username": "someone",
    "password": "password123",
    "login_count": 3,
    "roles": [
        "developer",
        "admin"
    ]
}
```
