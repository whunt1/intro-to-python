from timer import timer_func
import time

@timer_func
def print_some_stuff():
	some_list = ['a','b','c']
	for i in some_list:
		time.sleep(1)
		print(i)

if __name__ == '__main__': 
	print_some_stuff()