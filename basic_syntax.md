# Intro to Python

## Basic Syntax

### Built-in Types

```python
## Basic Data Types
type_string = "This is a string!"   # you can also use '' single quotes interchangably
type_int = 42
type_float = 3.14159                
type_bool = True                    # or False
type_None = None

# Compound Data Types
type_list = ["apples", 4, True]     # Mutable
type_tuple = 1, 2.1, 12, 154.4      # Immutable
type_set = {1, 2, 3}                # Unique Members
type_dict = {"a": 1, "b": 3}        # Key-Value Pairs

# Functions and Classes

def func(arg1, arg2, arg3):
    pass

class _class(parent_class):

    def member_func():
        pass

    pass

```

### Importing Modules
```python

import csv # imports 'csv' from the standard library

import pandas   # imports 'pandas' if installed, throws an error if not

import my_file  # imports 'my_file.py' if it can be found, throws an error if not

from typing import List # Imports ONLY the 'List' symbol from the typing module in the standard library

from matplotlib import pyplot as plt # Imports ONLY the 'pyplot' symbol from the matplotlib module, renaming it as 'plt' for convenience
```

### Control Flow
Python is whitespace-delimited. Where languages like C or Javascript would use a semicolon (';') to tell where one statement ends and another begins, Python relies on newlines and indentation.

```python
# For loops
for i in range(10):
    if i == 4:
        continue
    print(i) 

for c in "test":
    if c == 'e':
        break
    print(c)
    
for number in [1,2,3]:
    print(number * 2)
    
for key, val in {"a": 1, "b": 2, "c": 3}:
    print(key, val)
```
```python
# While Loops
heads_in_a_row = 0
while coin_flip() == "heads":
    heads_in_a_row += 1
```
```python
# If/Elif/Else
if False:
    pass
elif returns_false() and returns_true(): # short circuit! returns_true() is never run
    pass
else:
    pass
```

### Functions

Functions are defined using the "def" keyword.

```python
def func_name(arg1, arg2, arg3):
    pass
func_name(1,2,3)

# Default Args
def a_second_func(arg1, arg2 = False):
    pass
a_second_func(1)

# Unpacking args and keyword-args
def three_funcs(*args, **kwargs):
    pass
three_funcs(1, 2, 3, value=34, value2=45)
```

### Classes
Classes are defined using the "class" keyword.

```python
class Class_Name():

    class_name = "Class_Name"

    def __init__(self, number):
        self.member_value = number

    def member_func(self):
        print(self.member_value)

    def class_func(cls):
        print(f"This class's name is {cls.class_name}!")

a = Class_Name(34)
a.member_func()             # Prints '34'
Class_Name.class_func()     # Prints "This class's name is Class_Name!"
```

### Exceptions

functions in python can raise exceptions, meaning an error of some kind has been encountered.  These exceptions come in a variety of types and can be handled using the try/except syntax:

```python
while True:
    try:
        x = int(input("Please enter a number: "))
        break
    except ValueError:
        print("Oops!  That was no valid number.  Try again...")
```
(from: https://docs.python.org/3/tutorial/errors.html)